//
//  CameraCaptureViewController.m
//  pegasusdemo
//
//  Created by Michael R Traverso on 12/11/13.
//  Copyright (c) 2013 traversoft. All rights reserved.
//

#import "CameraCaptureViewController.h"
#import "RectangleView.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>

@interface CameraCaptureViewController ()
{
    BOOL hasImageCaptured, hasGrayScaleActive;
}
@property (strong, nonatomic) RectangleView *rectView;
@property(nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput;
@end

@implementation CameraCaptureViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        hasImageCaptured = NO;
        hasGrayScaleActive = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [alertView show];
    }
    else
    {
        _rectView = [[RectangleView alloc] initWithFrame:CGRectMake(_imageView.frame.origin.x,
                                                                   _imageView.frame.origin.y,
                                                                   _imageView.frame.size.width,
                                                                   _imageView.frame.size.height)];
        [self initializeCamera];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button handlers
- (IBAction)takePhoto: (UIButton *)sender
{
    if (sender == _btnClearCapturedPhoto)
    {
        [_imageOfTakenPhoto setImage:nil];
        [_imageOfTakenPhotoGray setImage:nil];
        [_btnClearCapturedPhoto setHidden:YES];
        [_btnColorGrayScale setHidden:YES];
        [_btnCropImage setEnabled:NO];
        [_btnSharePhoto setEnabled:NO];
        [_btnFinishCrop setHidden:YES];
    }
    else if (sender == _btnTakePhoto)
    {
        AVCaptureConnection *avConn = nil;
        for (AVCaptureConnection *connection in _stillImageOutput.connections)
        {
            
            for (AVCaptureInputPort *port in [connection inputPorts])
            {
                if ([[port mediaType] isEqual:AVMediaTypeVideo])
                {
                    avConn = connection;
                    break;
                }
            }

            if (avConn)
            {
                break;
            }
        }
        
        [_stillImageOutput
            captureStillImageAsynchronouslyFromConnection:avConn
            completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
             {
                if (imageSampleBuffer != NULL)
                {
                    NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
                    [self processImage:[UIImage imageWithData:imageData]];
                }
             }];
        
        [self prepareViewForPhotoSelected];
    }
}

- (IBAction)sharePhoto:(UIButton *)sender
{
    NSString *emailSubject = @"Photo from Take-A-Photo, Crop a Photo";
    
    UIImage *imgToSend = nil;
    if ([_imageOfTakenPhoto isHidden])
    {
        imgToSend = _imageOfTakenPhotoGray.image;
    }
    else
    {
        imgToSend = _imageOfTakenPhoto.image;
    }
    
    NSData *imageData = UIImagePNGRepresentation(imgToSend);

    MFMailComposeViewController *mailComposerVc = [[MFMailComposeViewController alloc] init];
    [mailComposerVc setMailComposeDelegate: self];
    [mailComposerVc setSubject:emailSubject];
    [mailComposerVc addAttachmentData:imageData mimeType:@"image/png" fileName:@"savedImage"];
    [self presentViewController:mailComposerVc animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", error);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)tapRecognized:(id)sender
{
    [_tutorialView setHidden:YES];
    [_imageView setImage:[UIImage imageNamed:@"yourImageHere.png"]];
    [self prepareViewForNoPhoto];
}

- (IBAction)switchColorGray:(id)sender
{
    hasGrayScaleActive = !hasGrayScaleActive;
    [_imageOfTakenPhotoGray setHidden:!hasGrayScaleActive];
    [_imageOfTakenPhoto setHidden:hasGrayScaleActive];
}

- (IBAction)cropPhoto: (UIButton *)sender
{
    if (sender == _btnCropImage)
    {
        if (_imageView && [self.view.subviews containsObject:_rectView])
        {
            [_rectView removeFromSuperview];
        }
        [self.view insertSubview:_rectView aboveSubview:_imageOfTakenPhoto];
        [_btnFinishCrop setHidden:NO];
    }
    else if (sender == _btnFinishCrop)
    {
        CGRect cropRect = CGRectMake(_rectView.selectedFrame.origin.x,
                                     _rectView.selectedFrame.origin.y,
                                     _rectView.selectedFrame.size.width,
                                     _rectView.selectedFrame.size.height);
        CGImageRef imageRef = CGImageCreateWithImageInRect([_imageOfTakenPhoto.image CGImage], cropRect);
        [_imageOfTakenPhoto setImage:[UIImage imageWithCGImage:imageRef]];
        [_imageOfTakenPhotoGray setImage:[self createGrayscaleImg:_imageOfTakenPhoto.image]];
        CGImageRelease(imageRef);

        if (_imageView && [self.view.subviews containsObject:_rectView])
        {
            [_rectView removeFromSuperview];
        }
        [_btnFinishCrop setHidden:YES];
    }
}

#pragma mark - AV Methods
- (void) initializeCamera
{
    AVCaptureSession *avCaptureSession = [[AVCaptureSession alloc] init];
	avCaptureSession.sessionPreset = AVCaptureSessionPresetPhoto;
	
	AVCaptureVideoPreviewLayer *avCapturePreviewLayer =
            [[AVCaptureVideoPreviewLayer alloc] initWithSession:avCaptureSession];
    [avCapturePreviewLayer setVideoGravity:AVLayerVideoGravityResize];
	avCapturePreviewLayer.frame = _imageView.bounds;
	[_imageView.layer addSublayer:avCapturePreviewLayer];
	
    CALayer *viewLayer = [_imageView layer];
    [viewLayer setMasksToBounds:YES];
    
    CGRect bounds = [_imageView bounds];
    [avCapturePreviewLayer setFrame:bounds];
    
    NSArray *devices = [AVCaptureDevice devices];
    AVCaptureDevice *camera;
    for (AVCaptureDevice *device in devices)
    {
        if ([device hasMediaType:AVMediaTypeVideo])
        {
            if ([device position] == AVCaptureDevicePositionBack)
            {
                camera = device;
            }
        }
    }
    
    NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:camera
                                                                        error:&error];
    if (!input)
    {
        NSString *msg = [NSString stringWithFormat:@"ERROR: Sorry, we're having trouble opening the camera: %@", error];
        NSLog(@"%@",msg);
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:msg
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [alertView show];

        return;
    }
    
    [avCaptureSession addInput:input];
	
    _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    [_stillImageOutput setOutputSettings:[[NSDictionary alloc]
                                          initWithObjectsAndKeys: AVVideoCodecJPEG,
                                                                  AVVideoCodecKey, nil]];
    [avCaptureSession addOutput:_stillImageOutput];
	[avCaptureSession startRunning];
}

- (void) processImage:(UIImage *)image
{
    hasImageCaptured = YES;

    UIGraphicsBeginImageContext(CGSizeMake(_imageOfTakenPhoto.frame.size.width, _imageOfTakenPhoto.frame.size.height));
    [image drawInRect: CGRectMake(0, 0, _imageOfTakenPhoto.frame.size.width, _imageOfTakenPhoto.frame.size.height)];
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGRect cropRect = CGRectMake(0, 0, _imageOfTakenPhoto.frame.size.width, _imageOfTakenPhoto.frame.size.height);
    CGImageRef imageRef = CGImageCreateWithImageInRect([smallImage CGImage], cropRect);
    [_imageOfTakenPhoto setImage:[UIImage imageWithCGImage:imageRef]];
    [_imageOfTakenPhotoGray setImage:[self createGrayscaleImg:_imageOfTakenPhoto.image]];
    CGImageRelease(imageRef);
}

#pragma mark - Helper methods
- (UIImage*)createGrayscaleImg:(UIImage*)imgToConvertToGrayscale
{
    int red = 1, blue = 2, green = 3;
    
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, imgToConvertToGrayscale.size.width * imgToConvertToGrayscale.scale, imgToConvertToGrayscale.size.height * imgToConvertToGrayscale.scale);
    
    int imgWidth = imageRect.size.width;
    int imgHeight = imageRect.size.height;
    
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(imgWidth * imgHeight * sizeof(uint32_t));
    
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, imgWidth * imgHeight * sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels,
                                                 imgWidth,
                                                 imgHeight,
                                                 8,  // 8-bit
                                                 imgWidth * sizeof(uint32_t),
                                                 colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    // paint the bitmap to our context which will fill in the pixels array
    CGContextDrawImage(context,
                       CGRectMake(0, 0, imgWidth, imgHeight),
                       [imgToConvertToGrayscale CGImage]);
    
    for(int xCoordPixel = 0; xCoordPixel < imgWidth; xCoordPixel++)
    {
        for(int yCoordPixel = 0; yCoordPixel < imgHeight; yCoordPixel++)
        {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[yCoordPixel * imgWidth + xCoordPixel];
            
            uint8_t gray = (uint8_t) ((30 * rgbaPixel[red] + 59 * rgbaPixel[green] + 11 * rgbaPixel[blue]) / 100);
            
            // set the pixels to gray
            rgbaPixel[red] = gray;
            rgbaPixel[blue] = gray;
            rgbaPixel[green] = gray;
        }
    }
    
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
    
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
    
    // make a new UIImage to return
    UIImage *grayImage = [UIImage imageWithCGImage:image
                                                 scale:imgToConvertToGrayscale.scale
                                           orientation:UIImageOrientationUp];
    
    // we're done with image now too
    CGImageRelease(image);
    
    return grayImage;
}

- (void)prepareViewForPhotoSelected
{
    // Remove crop layer if added
    if (_imageView && [self.view.subviews containsObject:_rectView])
    {
        [_rectView removeFromSuperview];
    }
    
    [_btnClearCapturedPhoto setHidden:NO];
    [_btnCropImage setEnabled:YES];
    [_btnSharePhoto setEnabled:YES];
    [_btnColorGrayScale setHidden:NO];
}

- (void)prepareViewForNoPhoto
{
    [_btnClearCapturedPhoto setHidden:YES];
    [_btnCropImage setEnabled:NO];
    [_btnSharePhoto setEnabled:NO];
    [_btnColorGrayScale setHidden:YES];
}

@end
