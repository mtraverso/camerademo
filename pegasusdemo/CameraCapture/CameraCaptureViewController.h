//
//  CameraCaptureViewController.h
//  pegasusdemo
//
//  Created by Michael R Traverso on 12/11/13.
//  Copyright (c) 2013 traversoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface CameraCaptureViewController : UIViewController <UIImagePickerControllerDelegate, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIImageView *imageOfTakenPhoto;
@property (strong, nonatomic) IBOutlet UIImageView *imageOfTakenPhotoGray;
@property (strong, nonatomic) IBOutlet UIImageView *cameraView;
@property (strong, nonatomic) IBOutlet UIButton *btnCropImage;
@property (strong, nonatomic) IBOutlet UIButton *btnFinishCrop;
@property (strong, nonatomic) IBOutlet UIButton *btnTakePhoto;
@property (strong, nonatomic) IBOutlet UIButton *btnSharePhoto;
@property (strong, nonatomic) IBOutlet UIButton *btnClearCapturedPhoto;
@property (strong, nonatomic) IBOutlet UIButton *btnColorGrayScale;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureRecognizer;
@property (strong, nonatomic) IBOutlet UIView *tutorialView;

- (IBAction)takePhoto: (UIButton *)sender;
- (IBAction)cropPhoto: (UIButton *)sender;
- (IBAction)sharePhoto:(UIButton *)sender;
- (IBAction)tapRecognized:(id)sender;
- (IBAction)switchColorGray:(id)sender;


//- (IBAction)selectPhoto:(UIButton *)sender;

@end
