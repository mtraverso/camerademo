//
//  main.m
//  pegasusdemo
//
//  Created by Michael R Traverso on 12/10/13.
//  Copyright (c) 2013 traversoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
