//
//  Constants.h
//  pegasusdemo
//
//  Created by Michael R Traverso on 12/10/13.
//  Copyright (c) 2013 traversoft. All rights reserved.
//

#ifndef pegasusdemo_Constants_h
#define pegasusdemo_Constants_h

#define IS_DEVICE_IPAD      [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad
#define IS_DEVICE_IPHONE5   [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && ([[UIScreen mainScreen] bounds].size.height * [[UIScreen mainScreen] scale] >= 1136)

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#endif
