//
//  RectangleView.m
//  pegasusdemo
//
//  Created by Michael R Traverso on 12/11/13.
//  Copyright (c) 2013 traversoft. All rights reserved.
//

#import "RectangleView.h"

@interface RectangleView ()
@property (nonatomic) BOOL touched;
@property (nonatomic) CGPoint locationOfTouchStart;
@property (nonatomic) CGPoint locationOfTouchEnd;
@end


@implementation RectangleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    self.touched = YES;
    UITouch *touch = [touches anyObject];
    self.locationOfTouchStart = [touch locationInView:self];
//    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    self.touched = YES;
    UITouch *touch = [touches anyObject];
    self.locationOfTouchEnd = [touch locationInView:self];
    
    _selectedFrame = CGRectMake(_locationOfTouchStart.x,
                                _locationOfTouchStart.y,
                                abs((_locationOfTouchEnd.x - _locationOfTouchStart.x)),
                                abs((_locationOfTouchEnd.y - _locationOfTouchStart.y)));
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    if (self.touched)
    {        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetLineWidth(context, 3);
        
        CGContextMoveToPoint(context, self.locationOfTouchStart.x-1.1, self.locationOfTouchStart.y);
        CGContextAddLineToPoint(context, self.locationOfTouchEnd.x, self.locationOfTouchStart.y);
        CGContextAddLineToPoint(context, self.locationOfTouchEnd.x, self.locationOfTouchEnd.y);
        CGContextAddLineToPoint(context, self.locationOfTouchStart.x, self.locationOfTouchEnd.y);
        CGContextAddLineToPoint(context, self.locationOfTouchStart.x, self.locationOfTouchStart.y);

        CGContextSetRGBFillColor(context, 255, 255, 255, 1);
        CGContextSetRGBStrokeColor(context, 255, 255, 255, 1);
        CGContextStrokePath(context);
    }
}


@end
