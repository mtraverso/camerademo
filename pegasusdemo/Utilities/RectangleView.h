//
//  RectangleView.h
//  pegasusdemo
//
//  Created by Michael R Traverso on 12/11/13.
//  Copyright (c) 2013 traversoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RectangleView : UIView
@property (nonatomic) CGRect selectedFrame;
@end
