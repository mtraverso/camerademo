//
//  AppDelegate.h
//  pegasusdemo
//
//  Created by Michael R Traverso on 12/10/13.
//  Copyright (c) 2013 traversoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplashViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
