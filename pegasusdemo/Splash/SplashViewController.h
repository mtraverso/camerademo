//
//  SplashViewController.h
//  pegasusdemo
//
//  Created by Michael R Traverso on 12/11/13.
//  Copyright (c) 2013 traversoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CameraCaptureViewController.h"

@interface SplashViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imgCameraShutter;

@end
