//
//  SplashViewController.m
//  pegasusdemo
//
//  Created by Michael R Traverso on 12/11/13.
//  Copyright (c) 2013 traversoft. All rights reserved.
//

#import "SplashViewController.h"

#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * 22/7)

@interface SplashViewController ()
{
    CGAffineTransform transform, finalTransform;
}
@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self animateSplash];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)animateSplash
{
    finalTransform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(1600));

    [UIView animateWithDuration:1.0 delay:0.05 options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _imgCameraShutter.transform = finalTransform;
                     }
                     completion:^(BOOL finished){
                         [self showMainNavigation];
                     }];

}

- (void)showMainNavigation
{
    [NSThread sleepForTimeInterval:1.2f];
    
    NSString *xib;
    if (IS_DEVICE_IPAD)
        xib = @"CameraCaptureView-iPad";
    else if (IS_DEVICE_IPHONE5)
        xib = @"CameraCaptureView-iPhone5";
    else
        xib = @"CameraCaptureView-iPhone4";
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    CameraCaptureViewController *camCaptureVc = [[CameraCaptureViewController alloc] initWithNibName:xib bundle:nil];
    [[[UIApplication sharedApplication] delegate].window setRootViewController: camCaptureVc];
}

@end
